flask-blow
==========

Install
-------

::

    pip install flask-blow


Usage
-----

flask-blow is a command line tool to setup flask application.

::

    $ cd myproject
    $ flask-blow
    $ make
    $ tree -L 2
    .
    ├── Makefile
    ├── README.rst
    ├── pip-requirements.txt
    ├── src
    │   ├── main.py
    │   └── static
    └── venv
        ├── bin
        ├── include
        ├── lib
        └── pyvenv.cfg

    6 directories, 5 files

You can run local server by::

    $ venv/bin/python src/main.py
