# coding: utf8

from flask import Flask, redirect, url_for
app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route('/favicon.ico')
def favicio():
    return redirect(url_for('static', filename='favicon.ico'))


if __name__ == "__main__":
    app.debug = True
    app.run()
