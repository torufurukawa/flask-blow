"""Flask application factory"""

import os
import os.path
import shutil
from pathlib import Path


def main(target_dir):
    template_dir = os.path.join(os.path.dirname(__file__), 'templates')
    for root_dir, dirnames, filenames in os.walk(template_dir):
        dst_dir = os.path.join(target_dir, root_dir[len(template_dir) + 1:])
        create_dirs(dst_dir, dirnames)
        copy_files(root_dir, dst_dir, filenames)


def create_dirs(dst_dir, dirnames):
    for dirname in dirnames:
        path_to_dir = Path(dst_dir).joinpath(dirname)
        create_dir(path_to_dir)


def create_dir(path_to_dir):
    """create a directory

    path_to_dir is a Path object
    """
    if path_to_dir.exists():
        return
    path_to_dir.mkdir(parents=True)


def copy_files(src_dir, dst_dir, filenames):
    for filename in filenames:
        src = Path(src_dir).joinpath(filename)
        dst = Path(dst_dir).joinpath(filename)
        copy_file(src, dst)


def copy_file(src, dst):
    """copy src file to dst

    src and dst are Path objects
    """
    if dst.exists():
        return
    if str(dst).endswith('.DS_Store'):
        return
    shutil.copy(str(src), str(dst))
