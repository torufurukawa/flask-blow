from distutils.core import setup
setup(name='flask-blow',
      version='0.1',
      packages=['flaskblow'],
      package_dir={'flaskblow': 'flaskblow'},
      package_data={'flaskblow': ['templates/*', 'templates/*/*',
                                  'templates/*/*/*']},
      scripts=['flask-blow'],
      )
